import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () =>{
  return(
    <container className={'footer'}>
      <article className={'footer__list'}><span className={'footer__text'}>(C) Maije 2019</span></article>
      <article className={'footer__list'}><Link to={'#'} className={'footer__link'}>Политика конфидициальности</Link></article>
    </container>
  )
};
export default Footer;