import React, { Component } from 'react';
import logo from './logo.svg';
import { Link } from 'react-router-dom';
import { Modal } from 'antd';
import PayModal from './components/PayModal';

class Header extends Component{
  state={
    isAvatar: true,
    isAuthenticated: false,

  };


  render() {
    const { isAuthenticated } = this.props;
    const {isAvatar} = this.state;
    const Avatar={
      backgroundImage: 'url(https://cdn.onlinewebfonts.com/svg/img_568657.png)',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPositionY: '5px',
      border: '1px solid black',
    };
    const style={
      background: 'url(https://sun9-26.userapi.com/c850628/v850628166/1b6807/cCe7rKANhGc.jpg)',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
    };

    if(isAuthenticated){
      return(
        <header className={'header'}>
          <container className={'header__content'}>
            <article className={'header__section'}>
             <PayModal />
              <span className={'header__item header__warning'}>Осталось 3 бесплатных дня</span>
            </article>
            <article className={'header__section'}>
              <div className={'header__list'}>
                <span className={'header__item header__name'}>Лундин Артём</span>
                <Link to={'#'} className={'header__item header__link'}>Личный кабинет</Link>
              </div>
              <Link to="/sign"><div className={'header__image'} style={isAvatar ? style : Avatar} /></Link>
            </article>
          </container>
        </header>
      )
    }else{
      return(
        <header className={'header-unlog'}>
          <container className={'header__content'}>
            <article className={'header__section'}>
              <img src={logo} alt="asd"/>
            </article>
            <article className={'header__section'}>
              <Link to={'#'} className={'header-unlog__link'}>Возможности</Link>
              <Link to={'#'} className={'header-unlog__link'}>О сервисе</Link>
              <Link to={'#'} className={'header-unlog__link'}>Мы в соц.сетях</Link>
            </article>
            <article className={'header__section'}>
              <Link to={'Sign'} onClick={this.props.SelectLogin} className={'header-unlog__link header-unlog__sign'}>Вход</Link>
              <Link to={'Sign'} onClick={this.props.SelectRegistration} className={'header-unlog__button'}>Регистрация</Link>
            </article>
          </container>
        </header>
      )
    }
  }
}
export default Header;