import React, { Component } from 'react';
import logo from '../logo.svg';

class PayCard extends Component{

  render() {
    let {price, title, sale} = this.props;

    return(
      <article className={'card'}>
        <div className={'card__content'}>
          <span className={'logo-block'}>
            <span className={'logo-animation before'}/>
            <span className={'logo-animation after'}/>
            <span className={'logo-animation content'}/>
          </span>

          <div className={'card__header'}>
            <span className={'card__content__title'}>Оформить подписку</span>
          </div>

          <img src={logo} alt="Majie" className={'card__logo'}/>
          <div className={'card-content'}>
            <span className={'card-content__item'}>{price}</span>
            <span className={'card-content__item card-content__desc'}>рублей</span>
            {!!sale && <span className={'card-content__item sale-line'}>{sale}</span>}
          </div>
        </div>
        <div className={'card__footer'}>
          <div className={'card__text'}>
            <span className={'card__title'}>{title}</span>
            <span className={'card__price'}>{price}</span>
          </div>
          <svg viewBox="0 0 28 25">
            <path fill="#fff"
                  d="M13.145 2.13l1.94-1.867 12.178 12-12.178 12-1.94-1.867 8.931-8.8H.737V10.93h21.339z"/>
          </svg>
        </div>

        <div className={'card__preview'}>
          <span className={'card__preview__title'}>{title}</span>
          <span className={'card__preview__price'}>{price}</span>
        </div>
      </article>
    )
  }
}
export default PayCard;