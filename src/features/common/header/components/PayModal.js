import React,{Component} from 'react';
import { Modal } from 'antd';
import PayCard from './PayCard';

export default class PayModal extends Component{
  state={
    visible: false
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  render() {
    return(
      <span className={'header__pay'}>
        <button className={'header__button'} onClick={this.showModal}>Оплатить</button>
        <Modal
          footer={null}
          title={null}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          bodyStyle={{width: '1140px'}}
          width={'1140px'}
        >
          <container className={'modal-pay'}>
            <h1 className={'modal-pay__header'}>Оплата</h1>
            <section className={'modal-pay__description'}>
              <article className={'modal-pay__block'}>
                <span className={'modal-pay__item'}>Полный доступ к редактору и всем шаблонам</span>
                <span className={'modal-pay__item'}>Еженедельное обновление шаблонов</span>
                <span className={'modal-pay__item'}>Возможность создания гиф-анимации</span>
                <span className={'modal-pay__item'}>Обучающий материал по дизайну</span>
                <span className={'modal-pay__item'}>Сохранение одной картинки сразу под разные размеры</span>
              </article>
              <article className={'modal-pay__block'}>
                <h2 className={'modal-pay__title'}>Введи промокод и получи скидку на один из пакетов:</h2>
                <div className={'modal-pay__tickets'}>
                  <input type="text" className={'modal-pay__input'} placeholder={'Промокод'}/>
                  <button className={'modal-pay__button'}>Применить</button>
                </div>
              </article>
            </section>
            <section className={'modal-pay__cards'}>
                <PayCard title={'1 месяц'} price={'499'} sale={null}/>
                <PayCard title={'3 месяца'} price={'980'} sale={'1280'}/>
                <PayCard title={'6 месяцев'} price={'2480'} sale={null}/>
            </section>
          </container>
        </Modal>
      </span>
    )
  }

}