import React from 'react';

const ArrowIcon = () =>{
  return(
    <div className={'arrow'}>
      <div className={'arrow__content'}/>
    </div>
  )
};
export default ArrowIcon;