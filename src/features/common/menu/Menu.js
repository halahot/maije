import React, { Component } from 'react';
import logo from '../header/logo.svg';
import { Link } from 'react-router-dom';

class Menu extends Component{
  state={
    isAvatar: false,
    isAuthenticated: false
  };

  render() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated) {
      return (
        <nav className={'nav'}>
          <article className={'nav__logo'}>
            <img src={logo} alt="Majie"/>
          </article>
          <article className={'nav__list'}>
            <Link to={'#'} className={'nav__item'}> Главная</Link>
            <Link to={'#'} className={'nav__item'}> Макеты</Link>
            <Link to={'#'} className={'nav__item'}> Шаблоны</Link>
            <Link to={'#'} className={'nav__item'}> Мои шаблоны</Link>
            <Link to={'#'} className={'nav__item'}> История</Link>
          </article>
        </nav>
      )
    }else{
      return(
        <div style={{display: 'none'}} />
      )
    }
  }
};
export default Menu;