import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from './redux/actions';
import "./css/design_main.css";
import {Link} from "react-router-dom";
import Editor from "./js/Editor";
import {Source} from "./js/Source";
import Back from "./components/Back";
import Front from "./components/Front";
import Save from "./components/Save";
import PopUp from "./components/PopUp";
import Zoom from "./components/Zoom";
import Instagram from "./components/Instagram";
import Vk from "./components/Vk";
import Facebook from "./components/Facebook";
import Templates from "./components/Templates";
import Objects from "./components/Objects";
import Download from "./components/Download";
import YourSvg from './css/1.svg'
import YourSvg2 from './css/common.svg'
import ReactSVG from "react-svg";
import {ChromePicker} from 'react-color'
import ColorPicker_img from './css/img/ColorPicker.png'

export class DefaultPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            popUp_template: false,
            popUp_object: false,
            popUp_upload: false,
            instagram_p: false,
            vk_p: false,
            facebook_p: false,
            instagram_sub_1: false,
            instagram_sub_2: false,
            instagram_sub_3: false,
            vk_sub_1: false,
            vk_sub_2: false,
            vk_sub_3: false,
            facebook_sub_1: false,
            facebook_sub_2: false,
            facebook_sub_3: false,
            stage: React.createRef(),
            for_svg: React.createRef(),
            objectDrag: false,
            objectLastPosX: 0,
            objectLastPosY: 0,
            drag: false,
            svg: YourSvg,
            displayColorPicker: false,
            prev_selected: null,
            color: {
                r: '241',
                g: '112',
                b: '19',
                a: '1',
            },


            history: [],
            current_pos: 0
        };
        this.selected = null;
        this.selection = document.createElement('span');

        document.addEventListener("mouseup", _ => {
            this.selected = null;
        });
        console.log(this.state.svg);
    }

    downloadFile = () => {
        let {saveSvgAsPng} = require("save-svg-as-png");
        let svg = document.querySelector('.for_svg svg').cloneNode(true);
        console.log(svg);
        svg.setAttribute('width', "1920");
        svg.setAttribute('height', "1080");
        saveSvgAsPng(svg, "image.png");
    };

    changeSize = (event, el, el2) => {
        if (event === this.state.stage.current) {
            return;
        } else if (this.state.drag === true) {
            let lp = {
                x: this.state.objectLastPosX,
                y: this.state.objectLastPosY
            };

            let cp = {
                x: event.clientX - lp.x,
                y: event.clientY - lp.y
            };

            console.log(lp);
            console.log(cp);
            console.log("width: " + this.state.prev_selected.getBoundingClientRect().width);
            this.state.prev_selected.setAttribute('width', this.state.prev_selected.getBoundingClientRect().width + cp.x);
            el2.width = this.state.prev_selected.getBoundingClientRect().width + cp.x;
        }

        this.setState({objectLastPosX: event.clientX})
        // this.state.stage.current.childNodes[[...this.state.stage.current.childNodes].indexOf(this.state.prev_selected)].setAttribute("width", "100");
    };

    changeTextColor = () => {
        // this.setState({displayColorPicker: true});
        // this.state.prev_selected.parentNode.childNodes.forEach(el=>{
        //     el.style.fill = "red";
        // })
    };

    handleClick = () => {
        this.setState({displayColorPicker: !this.state.displayColorPicker})
    };

    handleClose = () => {
        this.setState({displayColorPicker: false})
    };

    handleChange = (color) => {
        // console.log(color);
        if (this.state.prev_selected.tagName === "tspan") {
            this.setState({color: color.rgb}, _ => {
                this.state.prev_selected.parentNode.childNodes.forEach(el => {
                    el.style.fill = `rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, ${color.rgb.a})`;
                })
            });
        } else if (this.state.prev_selected.tagName === "text") {
            this.state.prev_selected.style.fill = `rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, ${color.rgb.a})`;
        }
    };

    handleChangeFontSize = event => {
        console.log(event.target.value);
        if (this.state.prev_selected.tagName === "tspan") {
            // this.setState()
            this.state.prev_selected.parentNode.childNodes.forEach(el => {
                el.style.fontSize = event.target.value + "em";
            })
        } else if (this.state.prev_selected.tagName === "text") {
            this.state.prev_selected.style.fontSize = event.target.value + "em";
        }
    };

    handleFontBold = _ => {
        if (this.state.prev_selected.tagName === "tspan") {
            // this.setState()
            this.state.prev_selected.parentNode.childNodes.forEach(el => {
                if (el.style.fontWeight !== "bold" && el.style.fontWeight !== "600") {
                    el.style.fontWeight = "bold";
                } else {
                    el.style.fontWeight = "normal";
                }
            })
        } else if (this.state.prev_selected.tagName === "text") {
            if (this.state.prev_selected.style.fontWeight !== "bold" && this.state.prev_selected.style.fontWeight !== "600") {
                this.state.prev_selected.style.fontWeight = "bold";
            } else {
                this.state.prev_selected.style.fontWeight = "normal";
            }
        }
    };

    handleFontStyle = _ => {
        if (this.state.prev_selected.tagName === "tspan") {
            // this.setState()
            this.state.prev_selected.parentNode.childNodes.forEach(el => {
                if (el.style.fontStyle !== "italic") {
                    el.style.fontStyle = "italic";
                } else {
                    el.style.fontStyle = "normal";
                }
            })
        } else if (this.state.prev_selected.tagName === "text") {
            if (this.state.prev_selected.style.fontStyle !== "italic") {
                this.state.prev_selected.style.fontStyle = "italic";
            } else {
                this.state.prev_selected.style.fontStyle = "normal";
            }
        }
    };

    handleTextTransform = _ => {
        if (this.state.prev_selected.tagName === "tspan") {
            // this.setState()
            this.state.prev_selected.parentNode.childNodes.forEach(el => {
                if (el.style.textTransform !== "uppercase" && el.style.textTransform !== "lowercase") {
                    el.style.textTransform = "uppercase";
                } else if (el.style.textTransform === "uppercase") {
                    el.style.textTransform = "lowercase";
                } else {
                    el.style.textTransform = "inherit";
                }
            })
        } else if (this.state.prev_selected.tagName === "text") {
            if (this.state.prev_selected.style.textTransform !== "uppercase" && this.state.prev_selected.style.textTransform !== "lowercase") {
                this.state.prev_selected.style.textTransform = "uppercase";
            } else if (this.state.prev_selected.style.textTransform === "uppercase") {
                this.state.prev_selected.style.textTransform = "lowercase";
            } else {
                this.state.prev_selected.style.textTransform = "inherit";
            }
        }
    };

    loadFile = _ => {
        let form = document.createElement('form');
        form.style.display = "none";
        document.body.appendChild(form);

        let input = document.createElement('input');
        input.style.display = "none";
        input.type = "file";
        input.onchange = event => {
            let file = input.files[0];

            console.log(input.value);
            console.log(file);

            let tmppath = URL.createObjectURL(file);


            console.log(tmppath);

            this.setState({svg: tmppath});

            form.reset();
        };
        form.appendChild(input);
        input.click();
    };

    changeColor = (color) => {
        this.setState(prevState => {
            let stage = Object.assign({}, prevState.stage);
            stage.current.childNodes[[...this.state.stage.current.childNodes].indexOf(this.state.prev_selected)].style.fill = color;
            return {stage};
        });
    };

    changeSvg = svg_ => {

        console.log(svg_);

        this.setState({svg: svg_})
    };


    updateSelection = (selection, element, sizeChanger) => {

        if (element === this.state.stage.current) {
            selection.style.display = 'none';
            sizeChanger.style.display = 'none';
            return;
        }
        let rect = element.getBoundingClientRect();

        selection.style.left = rect.left + window.scrollX + 'px';
        selection.style.top = rect.top + window.scrollY + 'px';
        selection.style.width = rect.width + 'px';
        selection.style.height = rect.height + window.scrollY + 'px';
        selection.style.display = 'block';

        sizeChanger.style.left = rect.left + window.scrollX + 'px';
        sizeChanger.style.top = rect.top + window.scrollY + 'px';
        sizeChanger.style.width = rect.width + 'px';
        sizeChanger.style.height = '20px';
        sizeChanger.style.display = 'block';
    };

    componentDidMount() {
        this.selection.style.position = 'absolute';
        this.selection.style.display = 'block';
        this.selection.style.outline = 'solid 2px #99f';
        this.selection.style.pointerEvents = 'none';

        console.log(this.state.for_svg);

        this.state.for_svg.current.appendChild(this.selection);

    }

    render() {
        let stage = this.state.stage;

        let sizeChanger = document.createElement('span');
        sizeChanger.style.position = 'absolute';
        sizeChanger.style.display = 'none';
        // sizeChanger.onmousedown = event => {
        //     this.setState({drag: true, objectLastPosX: event.clientX, objectLastPosY: event.clientY})
        // };
        // sizeChanger.onmouseleave = event => {
        //     this.selected = null;
        //     this.selection.style.display = 'none';
        //     sizeChanger.style.display = 'none';
        // };
        // sizeChanger.onmousemove = event => {
        //     this.changeSize(event, this.selection, sizeChanger);
        //     this.selected = null;
        //     this.selection.style.display = 'none';
        //     sizeChanger.style.display = 'none';
        // };
        // sizeChanger.onmouseup = _ => {
        //     this.setState({drag: false})
        // };
        // sizeChanger.style.borderTop = 'solid 10px #000';
        // document.body.appendChild(sizeChanger);

        let offset = {x: 0, y: 0};
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
        };

        return (
            <div className="design-default-page">
                <div className="head">
                    <div className="logo">
                        <Link to={"/"}>
                            <div className="back"/>
                        </Link>
                        <p className="logo_text">Редактор</p>
                    </div>
                    <div className="name_field">
                        <input type="text"/>
                    </div>
                    <div className="history_buttons">
                        <div className="back_b" onClick={_ => {
                            console.log(1);

                            if (this.state.current_pos === 0) {
                                this.setState({svg: this.state.history[this.state.history.length-2], current_pos: this.state.history.length-2})
                            }else{
                                this.setState({svg: this.state.history[this.state.current_pos-1], current_pos: this.state.current_pos-1})
                            }
                        }}>
                            <Back/>
                        </div>
                        <div className="front_b">
                            <Front/>
                        </div>
                    </div>
                    <div className="save_b" onClick={_ => this.downloadFile()}>
                        <span>Скачать <Save/></span>
                    </div>
                </div>
                <div className="body">
                    <div className="left_bar">
                        <ul>
                            <div className="pop_up popUp_template" onMouseLeave={_ => {
                                this.setState({popUp_template: false})
                            }} onMouseEnter={_ => {
                                this.setState({popUp_template: true})
                            }} style={{display: this.state.popUp_template ? "block" : "none"}}>
                                <PopUp/>
                                <span className={"stub"}/>
                                <div className="search_p">
                                    <Zoom/>
                                    <input type="text" placeholder={'Например "Instagram"'}/>
                                </div>
                                <div className="body_p">
                                    <div className="instagram_b" onClick={_ => {
                                        this.setState({
                                            instagram_p: !this.state.instagram_p,
                                            facebook_p: false,
                                            vk_p: false
                                        })
                                    }}>
                                        <Instagram/>
                                        Instagram
                                    </div>
                                    <div className={this.state.instagram_p ? "categories active_c" : "categories"}>
                                        <ul>
                                            <li onClick={_ => {
                                                this.setState({
                                                    instagram_sub_1: !this.state.instagram_sub_1,
                                                    instagram_sub_2: false,
                                                    instagram_sub_3: false,
                                                })
                                            }}>Пост 1080 х 1080 <span
                                                className={this.state.instagram_sub_1 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div
                                                className={this.state.instagram_sub_1 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item" onClick={event => {
                                                    this.changeSvg(event.target.parentNode.querySelector('img').src)
                                                }}>
                                                    <img src={YourSvg2} alt=""/>
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                            <li onClick={_ => {
                                                this.setState({
                                                    instagram_sub_2: !this.state.instagram_sub_2,
                                                    instagram_sub_1: false,
                                                    instagram_sub_3: false,
                                                })
                                            }}>Истории 1080 х 1920 <span
                                                className={this.state.instagram_sub_2 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div
                                                className={this.state.instagram_sub_2 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                            <li onClick={_ => {
                                                this.setState({
                                                    instagram_sub_3: !this.state.instagram_sub_3,
                                                    instagram_sub_1: false,
                                                    instagram_sub_2: false
                                                })
                                            }}>Карусель 1080 х 1920 <span
                                                className={this.state.instagram_sub_3 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div
                                                className={this.state.instagram_sub_3 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                    <div className="instagram_b" onClick={_ => {
                                        this.setState({vk_p: !this.state.vk_p, instagram_p: false, facebook_p: false})
                                    }}>
                                        <Vk/>
                                        Вконтакте
                                    </div>
                                    <div className={this.state.vk_p ? "categories active_c" : "categories"}>
                                        <ul>
                                            <li onClick={_ => {
                                                this.setState({
                                                    vk_sub_1: !this.state.vk_sub_1,
                                                    vk_sub_2: false,
                                                    vk_sub_3: false,
                                                })
                                            }}>Пост 1080 х 1080 <span
                                                className={this.state.vk_sub_1 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div className={this.state.vk_sub_1 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                            <li onClick={_ => {
                                                this.setState({
                                                    vk_sub_2: !this.state.vk_sub_2,
                                                    vk_sub_1: false,
                                                    vk_sub_3: false,
                                                })
                                            }}>Истории 1080 х 1920 <span
                                                className={this.state.vk_sub_2 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div className={this.state.vk_sub_2 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                            <li onClick={_ => {
                                                this.setState({
                                                    vk_sub_3: !this.state.vk_sub_3,
                                                    vk_sub_1: false,
                                                    vk_sub_2: false
                                                })
                                            }}>Карусель 1080 х 1920 <span
                                                className={this.state.vk_sub_3 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div className={this.state.vk_sub_3 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                    <div className="instagram_b" onClick={_ => {
                                        this.setState({
                                            facebook_p: !this.state.facebook_p,
                                            instagram_p: false,
                                            vk_p: false
                                        })
                                    }}>
                                        <Facebook/>
                                        Facebook
                                    </div>
                                    <div className={this.state.facebook_p ? "categories active_c" : "categories"}>
                                        <ul>
                                            <li onClick={_ => {
                                                this.setState({
                                                    facebook_sub_1: !this.state.facebook_sub_1,
                                                    facebook_sub_2: false,
                                                    facebook_sub_3: false
                                                })
                                            }}>Пост 1080 х 1080 <span
                                                className={this.state.facebook_sub_1 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div
                                                className={this.state.facebook_sub_1 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                            <li onClick={_ => {
                                                this.setState({
                                                    facebook_sub_2: !this.state.facebook_sub_2,
                                                    facebook_sub_1: false,
                                                    facebook_sub_3: false
                                                })
                                            }}>Истории 1080 х 1920 <span
                                                className={this.state.facebook_sub_2 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div
                                                className={this.state.facebook_sub_2 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                            <li onClick={_ => {
                                                this.setState({
                                                    facebook_sub_3: !this.state.facebook_sub_3,
                                                    facebook_sub_1: false,
                                                    facebook_sub_2: false,
                                                })
                                            }}>Карусель 1080 х 1920 <span
                                                className={this.state.facebook_sub_3 ? "sub_ sub_open" : "sub_ sub_close"}/>
                                            </li>
                                            <div
                                                className={this.state.facebook_sub_3 ? "sub_cat active_sub" : "sub_cat"}>
                                                <div className="item">
                                                    {/*<img src="https://picsum.photos/163/163" alt=""/>*/}
                                                    <p>Шаблон для поста в Instagram</p>
                                                    <span>1080 x 1080 пикс</span>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                                <div className="footer_p">

                                </div>
                            </div>
                            <li onMouseLeave={_ => {
                                this.setState({popUp_template: false})
                            }} onMouseEnter={_ => {
                                this.setState({popUp_template: true})
                            }}>
                                <Templates/>
                                <span>Шаблоны</span>
                            </li>
                            <div className="pop_up popUp_object" onMouseLeave={_ => {
                                this.setState({popUp_object: false})
                            }} onMouseEnter={_ => {
                                this.setState({popUp_object: true})
                            }} style={{display: this.state.popUp_object ? "block" : "none"}}>
                                <PopUp/>
                                <span className={"stub"}/>
                                <div className="search_p">
                                    {/*<input type="text"/>*/}
                                </div>
                                <div className="body_p">

                                </div>
                                <div className="footer_p">

                                </div>
                            </div>
                            <li onMouseLeave={_ => {
                                this.setState({popUp_object: false})
                            }} onMouseEnter={_ => {
                                this.setState({popUp_object: true})
                            }}>
                                <Objects/>
                                <span>Объекты</span>
                            </li>
                            <div className="pop_up popUp_upload" onMouseLeave={_ => {
                                this.setState({popUp_upload: false})
                            }} onMouseEnter={_ => {
                                this.setState({popUp_upload: true})
                            }} style={{display: this.state.popUp_upload ? "block" : "none"}}>
                                <PopUp/>
                                <span className={"stub"}/>
                                <div className="body_p">
                                    <input onClick={this.loadFile} type="text" defaultValue={"Загрузить"}/>
                                </div>
                            </div>
                            <li onMouseLeave={_ => {
                                this.setState({popUp_upload: false})
                            }} onMouseEnter={_ => {
                                this.setState({popUp_upload: true})
                            }}>
                                <Download/>
                                <span>Загрузить</span>
                            </li>
                        </ul>
                    </div>
                    <div className="editor">
                        <div className="editor_head">
                            {this.state.pr}
                            {this.state.prev_selected && (this.state.prev_selected.tagName === "tspan" || this.state.prev_selected.tagName === "text") ?
                                <div className="text-panel" onChange={this.handleChangeFontSize}>
                                    <select className={"fontChanger"}>
                                        <option value="2">2 em</option>
                                        <option value="4">4 em</option>
                                        <option value="6">6 em</option>
                                        <option value="8">8 em</option>
                                        <option value="10">10 em</option>
                                        <option value="12">12 em</option>
                                        <option value="14">14 em</option>
                                        <option value="16">16 em</option>
                                        <option value="18">18 em</option>
                                        <option value="20">20 em</option>
                                        <option value="24">24 em</option>
                                        <option value="28">28 em</option>
                                        <option value="32">32 em</option>
                                        <option value="40">40 em</option>
                                        <option value="48">48 em</option>
                                    </select>
                                    <img src={ColorPicker_img} alt="" onClick={this.handleClick}/>
                                    <span className={"boldChanger"}
                                          onClick={this.handleFontBold}><strong>B</strong></span>
                                    <span className={"styleChanger"} onClick={this.handleFontStyle}><i>I</i></span>
                                    <span className={"transformChanger"} onClick={this.handleTextTransform}>aA</span>
                                </div>
                                : null}
                            {this.state.displayColorPicker ? <div className={"color_picker"}>
                                <div style={cover} onClick={this.handleClose}/>
                                <ChromePicker color={this.state.color} onChange={this.handleChange}/></div> : null}
                        </div>
                        <div className="editor_body">
                            <div className="content">
                                <div className="for_svg" ref={this.state.for_svg}>
                                    <svg
                                        onMouseMove={event => {
                                            if (this.selected) {

                                                if (this.selected.tagName === 'circle') {

                                                    this.selected.setAttribute('cx', event.clientX + offset.x);
                                                    this.selected.setAttribute('cy', event.clientY + offset.y);

                                                } else if (this.selected.tagName === "path" || this.selected.tagName === "polygon") {

                                                    this.selected.style.transform = `translate(${event.clientX + offset.x}px, ${event.clientY + offset.y}px)`;
                                                } else {
                                                    this.selected.setAttribute('x', event.clientX + offset.x);
                                                    this.selected.setAttribute('y', event.clientY + offset.y);

                                                }
                                                this.updateSelection(this.selection, this.selected, sizeChanger);

                                            }
                                        }}
                                        onMouseUp={event => {
                                            this.selected = null;

                                            console.log(document.querySelector('.for_svg svg svg svg').innerHTML);

                                            let tmppath = URL.createObjectURL(new Blob([document.querySelector('.for_svg svg svg svg').innerHTML], {
                                                type: 'application/binary'
                                            }));


                                            this.setState({
                                                prev_selected: event.target,
                                                history: this.state.history.concat(tmppath),
                                                current_pos: 0
                                            })
                                        }
                                        }
                                        onMouseDown={event => {
                                            let target = event.target;

                                            if (!(target === stage.current)) {
                                                if (target.tagName === 'circle') {

                                                    offset.x = parseFloat(target.getAttribute('cx')) - event.clientX;
                                                    offset.y = parseFloat(target.getAttribute('cy')) - event.clientY;
                                                } else if (target.tagName === "path" || target.tagName === "polygon") {


                                                    if (target.style.transform) {
                                                        let text = target.style.transform;
                                                        text = text.replace(/px/g, "");
                                                        console.log(text.slice(text.indexOf("(") + 1, text.indexOf(")")).split(','));
                                                        text = text.slice(text.indexOf("(") + 1, text.indexOf(")")).split(',');
                                                        offset.x = parseFloat(text[0]) - event.clientX;
                                                        offset.y = parseFloat(text[1]) - event.clientY;
                                                    } else {
                                                        console.log(1);
                                                        offset.x = target.getBoundingClientRect().x - event.clientX - target.getBoundingClientRect().x;
                                                        offset.y = target.getBoundingClientRect().y - event.clientY - target.getBoundingClientRect().y;
                                                    }
                                                } else {

                                                    offset.x = parseFloat(target.getAttribute('x')) - event.clientX;
                                                    offset.y = parseFloat(target.getAttribute('y')) - event.clientY;
                                                }
                                                this.selected = target;
                                            }
                                        }
                                        } onMouseOver={event => {
                                        let target = event.target;


                                        if (!target.getAttribute('x')) {
                                            target.setAttribute('x', 0);
                                        }
                                        if (!target.getAttribute('y')) {
                                            target.setAttribute('y', 0);
                                        }

                                        this.updateSelection(this.selection, target, sizeChanger);
                                    }} ref={this.state.stage} id="stage" height="541" xmlns="http://www.w3.org/2000/svg"
                                        xmlnsXlink="http://www.w3.org/1999/xlink">
                                        {/*<rect id={1} x="233.95" y="172.7" width="150" height="310"*/}
                                        {/*      style={{stroke: "#000", fill: "rgb(224, 169, 174)"}}/>*/}
                                        {/*<rect id={2} x="233.95" y="172.7" width="150" height="130"*/}
                                        {/*      style={{stroke: "#000", fill: "rgb(224, 169, 174)"}}/>*/}
                                        <ReactSVG src={this.state.svg} wrapper="svg"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form id={"file_form"}>
                    <input type="file" className={"file_input"}/>
                </form>
            </div>
        );
    }
}

/* istanbul ignore next */
function mapStateToProps(state) {
    return {
        design: state.design,
    };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DefaultPage);
