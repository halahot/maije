import React from 'react';

function Facebook() {
    return (
        <svg width="11" height="24" viewBox="0 0 11 24" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path
                d="M7.31452 24V11.9986H10.5687L11 7.86284H7.31452L7.32005 5.79286C7.32005 4.71419 7.42072 4.13622 8.94255 4.13622H10.977V0H7.72228C3.81288 0 2.43687 2.00628 2.43687 5.38022V7.86331H0V11.9991H2.43687V24H7.31452Z"
                fill="#00DEB1"/>
        </svg>
    )
}

export default Facebook;