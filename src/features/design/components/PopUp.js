import React from 'react';

function PopUp() {
    return (
        <svg width="109" height="122" viewBox="0 0 109 122" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path
                d="M64 103C64 49.4001 21.3333 38.0001 0 39.0001L22.5 0L109 32.5C94 78.3334 64 156.6 64 103Z"
                fill="#00DEB1"/>
        </svg>
    )
}

export default PopUp;