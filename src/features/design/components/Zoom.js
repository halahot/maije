import React from 'react';

function Zoom() {
    return (
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path
                d="M7.53087 2.94788C7.01129 2.94788 6.58984 3.36964 6.58984 3.8889C6.58984 4.40911 7.01129 4.82993 7.53087 4.82993C8.68074 4.82993 9.70763 5.47217 10.2118 6.50724C10.375 6.84126 10.7097 7.03563 11.0582 7.03563C11.1969 7.03563 11.3374 7.00512 11.4699 6.94033C11.9372 6.71293 12.1316 6.14964 11.9036 5.68195C11.0817 3.99553 9.4057 2.94788 7.53087 2.94788Z"
                fill="white"/>
            <path
                d="M17.6553 15.8732L14.5193 12.7369C15.4477 11.431 15.9991 9.83924 15.9991 8.11822C15.9991 3.70714 12.4104 0.11853 7.99937 0.11853C3.5883 0.11853 0 3.70714 0 8.11822C0 12.529 3.5883 16.1176 7.99937 16.1176C9.84211 16.1176 11.5361 15.4854 12.8898 14.4346L15.9915 17.5364C16.2211 17.766 16.5227 17.8811 16.8234 17.8811C17.1247 17.8811 17.4257 17.7663 17.6553 17.5364C18.1148 17.0775 18.1148 16.3327 17.6553 15.8732ZM1.88269 8.11853C1.88269 4.74536 4.62714 2.00122 8 2.00122C11.3729 2.00122 14.1173 4.74536 14.1173 8.11853C14.1173 9.52189 13.6377 10.8127 12.8394 11.8462C12.5756 12.1874 12.2793 12.5022 11.9503 12.7812C10.8835 13.6864 9.50558 14.2358 8 14.2358C4.62683 14.2355 1.88269 11.4914 1.88269 8.11853Z"
                fill="white"/>
        </svg>
    )
}

export default Zoom;