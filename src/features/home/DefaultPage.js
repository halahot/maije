import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import Header from '../common/header/Header';
import Menu from '../common/menu/Menu';
import SectionCreateNewLayout from './components/sections/SectionCreateNewLayout';
import SectionMostPopular from './components/sections/SectionMostPopular';
import SectionTemplate from './components/sections/SectionTemplate';


export class HomePage extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  render() {

    return (
      <div className="home">
          <Menu isAuthenticated={true} />
        <div style={{width:'100%'}}>
          <Header isAuthenticated={true} />
          <container className={'home__content'}>

            <SectionCreateNewLayout />
            <SectionMostPopular />
            <SectionTemplate />

          </container>
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
