import React, { Component } from 'react';
import CreateLayoutBackground from './CreateLayoutBackground.svg';
import { Link } from 'react-router-dom';

class CreateLayout extends Component{

  render() {

    const styles={
      // backgroundImage: 'url('+ CreateLayoutBackground + ')',
    };

    return(
      <span className={'layout'} style={styles}>
        <img className={'layout__background'} src={CreateLayoutBackground} alt=""/>
        <div className={'layout__list'}>
          <Link to={"/design"}><button className={'layout__item'} >Создать новый макет</button></Link>
        </div>
      </span>
    )
  }

  goToDesigner() {
    this.props.history.push("/design")
  }

}
export default CreateLayout;