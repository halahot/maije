import React from 'react';
import VKIcon from '../../../common/icons/VKIcon';
import HandIcon from '../../../common/icons/HandIcon';

const MostPopularBlock =(props)=>{

    return(
      <article className={'most-popular'}>

        <div className={'most-popular__media'}>

          <div className={'most-popular__image'}>
            <span className={'most-popular__hover'}><HandIcon /></span>
            <VKIcon />
          </div>

          <div className={'most-popular__horizontal'}>
            <span className={'most-popular__info'}>{props.width} пикс </span>
          </div>
          <div className={'most-popular__vertical'}>
            <span className={'most-popular__info'}>{props.height} пикс </span>
          </div>
          <span className={'most-popular__arrow'} />

        </div>
        <div className={'most-popular__list'}>
          <span className={'most-popular__item'}>{props.title}</span>
          <span className={'most-popular__description'}>{props.width} х {props.height} пикс</span>
        </div>

      </article>
    )
};
export default MostPopularBlock;