import React from 'react';
import SearchIcon from '../../../common/icons/SearchIcon';

const Search=(props)=>{
  return(
    <span className={'maije-search'}>
      <SearchIcon className={'maije-search__ico'} />
      <input className={'maije-search__input'} placeholder={props.placeholder} type="text"/>
    </span>
  )
};
export  default Search;