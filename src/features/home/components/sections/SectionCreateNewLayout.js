import React, {Component} from 'react';
import CreateLayout from '../index';
import Search from '../search/Search';


class SectionCreateNewLayout extends Component{

  render() {

    return(
      <section className={'home__section'}>
        <article className={'home__block'}>
          <h1 className={'home__title'}>Создайте новый макет</h1>
          <Search
            placeholder="Например “Универсальная запись”"
            onSearch={value => console.log(value)}
          />
        </article>
        <article className={'home__block'}>
          <CreateLayout />
        </article>
      </section>
    )
  }
}
export default SectionCreateNewLayout;