import React from 'react';
import MostPopularBlock from '../mostPopular/MostPopularBlock';

const SectionMostPopular=()=>{
    return(
      <section className={'home__section'}>
        <article className={'home__block'}>
          <h1 className={'home__title'}>Часто используемые:</h1>
        </article>
        <article className={'home__items'}>

          <MostPopularBlock
          width={'1000'}
          height={'700'}
          title={'Картинка поста для ВК'}
          />
          {/* DELETE_FROM_HERE start */}
          <MostPopularBlock width={'1000'} height={'700'} title={'Картинка поста для ВК'}/>
          <MostPopularBlock width={'1000'} height={'700'} title={'Картинка поста для ВК'}/>
          <MostPopularBlock width={'1000'} height={'700'} title={'Картинка поста для ВК'}/>
          {/* DELETE_FROM_HERE end */}

        </article>
      </section>
    )
};
export default SectionMostPopular;