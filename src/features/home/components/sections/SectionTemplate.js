import React from 'react';
import TemplateBlock from '../template/TemplateBlock';
import template from '../template/template.svg'
import Instagram from "../svgs/instagram";
import Vk from "../svgs/vk";

const SectionTemplate = () => {
    return (
        <section className={'home__section'}>
            <article className={'home__block'}>
                <h1 className={'home__title'}>Шаблоны:</h1>
            </article>
            <article className={'home__social'}>
                <button><Instagram/>
                    Instagram
                </button>
            </article>
            <article className={'home__items'}>

                <TemplateBlock
                    width={'1080'}
                    height={'1080'}
                    title={'Шаблон для поста в Instagram'}
                    image={template}
                />
                {/* DELETE_FROM_HERE start */}
                <TemplateBlock width={'1080'} height={'1080'} title={'Шаблон для поста в Instagram'} image={template}/>
                <TemplateBlock width={'1080'} height={'1080'} title={'Шаблон для поста в Instagram'} image={template}/>
                <TemplateBlock width={'1080'} height={'1080'} title={'Шаблон для поста в Instagram'} image={template}/>
                <TemplateBlock width={'1080'} height={'1080'} title={'Шаблон для поста в Instagram'} image={template}/>
                {/* DELETE_FROM_HERE end */}

            </article>
            <article className={'home__social'}>
                <button><Vk/>Вконтакте</button>
            </article>
        </section>
    )
};
export default SectionTemplate;