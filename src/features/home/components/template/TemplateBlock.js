import React from 'react';

const TemplateBlock=(props)=>{
  return(
    <article className={'template most-popular'}>
      <div className={'template__image'}><img src={props.image} alt=""/></div>
      <div className={'most-popular__list'}>
        <span className={'most-popular__item'}>{props.title}</span>
        <span className={'most-popular__description'}>{props.width} х {props.height} пикс</span>
      </div>
    </article>
  )
};
export default TemplateBlock;