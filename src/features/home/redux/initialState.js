const initialState = {
  getTemplatesPending: false,
  getTemplatesError: null,
};

export default initialState;
