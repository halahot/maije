import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import Header from '../common/header/Header';
import BackGround from './components/BackGround';
import ModuleLogin from './components/ModuleLogin';
import ModuleRegistration from './components/ModuleRegistration';
import ModuleWelcome from './components/ModuleWelcome';
import Footer from '../common/footer/Footer';

export class DefaultPage extends Component {
  static propTypes = {
    sign: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state={
    tabInit: 'Welcome'
  };

  onSelectRegistrationBlock = () =>{
    this.setState({
      ...this.state,
      tabInit: "Registration",
    });
  };
  onSelectWelcomeBlock = () =>{
    this.setState({
      ...this.state,
      tabInit: "Welcome",
    });
  };
  onSelectLoginBlock = () =>{
    this.setState({
      ...this.state,
      tabInit: "Login",
    });
  };

  welcomeBlock(){
    return(
      <ModuleWelcome
        SelectRegistration={this.onSelectRegistrationBlock.bind(this)}
        SelectLogin={this.onSelectLoginBlock.bind(this)}
      />
    )
  }
  registerBlock(){
    return(
      <ModuleRegistration
        SelectWelcome={this.onSelectWelcomeBlock.bind(this)}
      />
    )
  }
  loginBlock(){
    return(
      <ModuleLogin SelectWelcome={this.onSelectWelcomeBlock.bind(this)}/>
    )
  }


  render() {
    let logIn;

    switch (this.state.tabInit) {
      case "Welcome":
        logIn = this.welcomeBlock();
        break;
      case "Registration":
        logIn = this.registerBlock();
        break;
      case "Login":
        logIn = this.loginBlock();
        break;

      default:
        logIn = this.welcomeBlock();
    }

    return (
      <section className="DELET_FROM_HERE">
          <Header
            SelectLogin={this.onSelectLoginBlock.bind(this)}
            SelectRegistration={this.onSelectRegistrationBlock.bind(this)}
            isAuthenticated={false}
          />
          <container className="sign">
            <article className={'sign__content'}>
              <BackGround />
            </article>
            <article className={'sign__content'}>
              {logIn}
            </article>
          </container>
        <Footer />
      </section>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    sign: state.sign,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultPage);
