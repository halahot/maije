import React from 'react';
import ArrowIcon from '../../common/icons/ArrowIcon';
import {Link} from 'react-router-dom';
const ModuleLogin =(props)=>{
  return(
    <div className={'sign__block'}>
      <h1 className={'sign__header profile-data__header'}><ArrowIcon /><span onClick={props.SelectWelcome}>Войти</span></h1>
      <button className={'sign__item'}>Войти через Вконтакте</button>
      <button className={'sign__item'}>Войти через Фасебук</button>
      <span className={'sign__misc'}>или</span>
      <input type={'text'} placeholder={'E - mail'} className={'sign__input'} />
      <input type={'password'} placeholder={'Пароль'} className={'sign__input'} />
      <Link to="/"><button
        className={'sign__button'}
      >Войти</button></Link>
    </div>
  )
};
export default ModuleLogin;
