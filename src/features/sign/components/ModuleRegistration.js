import React from 'react';
import ArrowIcon from '../../common/icons/ArrowIcon';
import 'antd/dist/antd.css';
import { Checkbox } from 'antd';
import {Link} from 'react-router-dom';

const ModuleRegistration =(props)=>{
  return(
    <div className={'sign__block profile-data'}>
      <h1 className={'sign__header profile-data__header'}><ArrowIcon /><span onClick={props.SelectWelcome}>Создай новый аккаунт</span></h1>
      <input type={'text'} placeholder={'E - mail'} className={'sign__input'} />
      <input type={'password'} placeholder={'Пароль'} className={'sign__input'} />
      <Link to="/"><button
        className={'sign__button profile-data__button'}
      >Начать работу</button></Link>


      <Checkbox >Я принимаю метаамфитамин</Checkbox>
    </div>
  )
};
export default ModuleRegistration;