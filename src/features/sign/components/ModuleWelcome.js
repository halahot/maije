import React from 'react';
import FacebookLogin from 'react-facebook-login';


const ModuleWelcome = (props) => {

    const callback = response => {
        // const { userId, name, email } = response;
        console.log(response);
        // handleCallback(userId, name, email);
    };

    return (
        <div className={'sign__block'}>
            <h1 className={'sign__header'}>Зарегистрируйся и iBash</h1>
            <button className={'sign__item'}><i className="fa fa-vk"/> Регистрация через Вконтакте</button>

            <FacebookLogin
                appId={498690704065161}
                callback={data => {
                    callback(data)
                }}
                textButton="Регистрация через Facebook"
                icon="fa-facebook"
                cssClass={'sign__item'}
            />
            <span className={'sign__misc'}>ИЛИ</span>
            <button
                className={'sign__button'}
                onClick={props.SelectRegistration}
            >Регистрация
            </button>
            <span className={'sign__text'}>Уже есть аккаунт?<span onClick={props.SelectLogin}
                                                                  className={'sign__link'}> Войти</span></span>
        </div>
    )
};
export default ModuleWelcome;