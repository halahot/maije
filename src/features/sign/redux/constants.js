export const SIGN_REGISTRATION_BEGIN = 'SIGN_REGISTRATION_BEGIN';
export const SIGN_REGISTRATION_SUCCESS = 'SIGN_REGISTRATION_SUCCESS';
export const SIGN_REGISTRATION_FAILURE = 'SIGN_REGISTRATION_FAILURE';
export const SIGN_REGISTRATION_DISMISS_ERROR = 'SIGN_REGISTRATION_DISMISS_ERROR';
export const SIGN_LOGIN_BEGIN = 'SIGN_LOGIN_BEGIN';
export const SIGN_LOGIN_SUCCESS = 'SIGN_LOGIN_SUCCESS';
export const SIGN_LOGIN_FAILURE = 'SIGN_LOGIN_FAILURE';
export const SIGN_LOGIN_DISMISS_ERROR = 'SIGN_LOGIN_DISMISS_ERROR';
export const SIGN_PAY_BEGIN = 'SIGN_PAY_BEGIN';
export const SIGN_PAY_SUCCESS = 'SIGN_PAY_SUCCESS';
export const SIGN_PAY_FAILURE = 'SIGN_PAY_FAILURE';
export const SIGN_PAY_DISMISS_ERROR = 'SIGN_PAY_DISMISS_ERROR';
