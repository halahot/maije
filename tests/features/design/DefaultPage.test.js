import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/design/DefaultPage';

describe('design/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      design: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.design-default-page').length
    ).toBe(1);
  });
});
