import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_GET_TEMPLATES_BEGIN,
  HOME_GET_TEMPLATES_SUCCESS,
  HOME_GET_TEMPLATES_FAILURE,
  HOME_GET_TEMPLATES_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  getTemplates,
  dismissGetTemplatesError,
  reducer,
} from '../../../../src/features/home/redux/getTemplates';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/getTemplates', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when getTemplates succeeds', () => {
    const store = mockStore({});

    return store.dispatch(getTemplates())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_GET_TEMPLATES_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_GET_TEMPLATES_SUCCESS);
      });
  });

  it('dispatches failure action when getTemplates fails', () => {
    const store = mockStore({});

    return store.dispatch(getTemplates({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_GET_TEMPLATES_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_GET_TEMPLATES_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissGetTemplatesError', () => {
    const expectedAction = {
      type: HOME_GET_TEMPLATES_DISMISS_ERROR,
    };
    expect(dismissGetTemplatesError()).toEqual(expectedAction);
  });

  it('handles action type HOME_GET_TEMPLATES_BEGIN correctly', () => {
    const prevState = { getTemplatesPending: false };
    const state = reducer(
      prevState,
      { type: HOME_GET_TEMPLATES_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getTemplatesPending).toBe(true);
  });

  it('handles action type HOME_GET_TEMPLATES_SUCCESS correctly', () => {
    const prevState = { getTemplatesPending: true };
    const state = reducer(
      prevState,
      { type: HOME_GET_TEMPLATES_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getTemplatesPending).toBe(false);
  });

  it('handles action type HOME_GET_TEMPLATES_FAILURE correctly', () => {
    const prevState = { getTemplatesPending: true };
    const state = reducer(
      prevState,
      { type: HOME_GET_TEMPLATES_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getTemplatesPending).toBe(false);
    expect(state.getTemplatesError).toEqual(expect.anything());
  });

  it('handles action type HOME_GET_TEMPLATES_DISMISS_ERROR correctly', () => {
    const prevState = { getTemplatesError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_GET_TEMPLATES_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getTemplatesError).toBe(null);
  });
});

