import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/sign/DefaultPage';

describe('sign/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      sign: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.sign-default-page').length
    ).toBe(1);
  });
});
