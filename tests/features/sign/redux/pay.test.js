import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  SIGN_PAY_BEGIN,
  SIGN_PAY_SUCCESS,
  SIGN_PAY_FAILURE,
  SIGN_PAY_DISMISS_ERROR,
} from '../../../../src/features/sign/redux/constants';

import {
  pay,
  dismissPayError,
  reducer,
} from '../../../../src/features/sign/redux/pay';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('sign/redux/pay', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when pay succeeds', () => {
    const store = mockStore({});

    return store.dispatch(pay())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', SIGN_PAY_BEGIN);
        expect(actions[1]).toHaveProperty('type', SIGN_PAY_SUCCESS);
      });
  });

  it('dispatches failure action when pay fails', () => {
    const store = mockStore({});

    return store.dispatch(pay({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', SIGN_PAY_BEGIN);
        expect(actions[1]).toHaveProperty('type', SIGN_PAY_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissPayError', () => {
    const expectedAction = {
      type: SIGN_PAY_DISMISS_ERROR,
    };
    expect(dismissPayError()).toEqual(expectedAction);
  });

  it('handles action type SIGN_PAY_BEGIN correctly', () => {
    const prevState = { payPending: false };
    const state = reducer(
      prevState,
      { type: SIGN_PAY_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.payPending).toBe(true);
  });

  it('handles action type SIGN_PAY_SUCCESS correctly', () => {
    const prevState = { payPending: true };
    const state = reducer(
      prevState,
      { type: SIGN_PAY_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.payPending).toBe(false);
  });

  it('handles action type SIGN_PAY_FAILURE correctly', () => {
    const prevState = { payPending: true };
    const state = reducer(
      prevState,
      { type: SIGN_PAY_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.payPending).toBe(false);
    expect(state.payError).toEqual(expect.anything());
  });

  it('handles action type SIGN_PAY_DISMISS_ERROR correctly', () => {
    const prevState = { payError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: SIGN_PAY_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.payError).toBe(null);
  });
});

